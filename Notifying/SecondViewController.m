//
//  SecondViewController.m
//  Notifying
//
//  Created by James Cash on 19-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) UIButton *button;
@property (nonatomic,strong) NSString *finishedText;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    self.textField = [[UITextField alloc]
                      initWithFrame:CGRectMake(10, 10, 150, 44)];
    self.button = [[UIButton alloc]
                   initWithFrame:CGRectMake(10, 54, 100, 44)];
    [self.button setTitle:@"Done" forState:UIControlStateNormal];

    [self.button addTarget:self
                    action:@selector(doneWithSelf:)
          forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.textField];
    [self.view addSubview:self.button];

    /*
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(imDone:)
     name:kSecondNotificationName
     object:self];
     */
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imDone:(NSNotification*)note
{
    NSLog(@"I'm done");
}

- (void)doneWithSelf:(id)sender
{
    /*
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kSecondNotificationName
     object:self
     userInfo:@{kSecondNotificationTextKey: self.textField.text}];
     */
    self.finishedText = self.textField.text;
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
