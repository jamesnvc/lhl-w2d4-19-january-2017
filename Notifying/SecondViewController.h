//
//  SecondViewController.h
//  Notifying
//
//  Created by James Cash on 19-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString* kSecondNotificationName = @"SecondVCDoneNotification";
static NSString* kSecondNotificationTextKey = @"newText";

@interface SecondViewController : UIViewController

@end
