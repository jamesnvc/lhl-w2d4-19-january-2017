//
//  ViewController.m
//  Notifying
//
//  Created by James Cash on 19-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *someLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /*
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(otherControllerDone:)
     name:kSecondNotificationName
     object:nil];
     */
    UIGestureRecognizer *gesture = [[UIPinchGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(pinchOnView:)];
    [self.view addGestureRecognizer:gesture];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showOtherField:(id)sender {
    SecondViewController *svc = [[SecondViewController alloc] init];
    svc.view.frame = CGRectInset(self.view.frame, 50, 50);
    [self addChildViewController:svc];
    [self.view addSubview:svc.view];
    [svc addObserver:self
          forKeyPath:@"finishedText"
             options:NSKeyValueObservingOptionNew
             context:nil/*(void*)@"this thing"*/];

}

- (void)pinchOnView:(UIGestureRecognizer*)gesture
{
    CGPoint pinchLoc = [gesture locationInView:self.view];
    NSString *pointStr = NSStringFromCGPoint(pinchLoc);
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            NSLog(@"Started pinch @ %@", pointStr);
            break;
        case UIGestureRecognizerStateChanged:
            NSLog(@"Pinch moving @ %@", pointStr);
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"Pinch ended");
            break;
        default:
            break;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
//    NSString *whichThing = (NSString*)context;
    NSLog(@"Observed key path %@ of %@; changes %@", keyPath, object, change);
    [object removeObserver:self forKeyPath:@"finishedText"];
    self.someLabel.text = change[NSKeyValueChangeNewKey];
}

-(void)otherControllerDone:(NSNotification*)notification
{
    NSLog(@"Notified: %@", notification);
    self.someLabel.text = notification.userInfo[kSecondNotificationTextKey];
}

@end
